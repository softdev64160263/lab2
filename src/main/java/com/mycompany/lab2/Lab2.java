/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab2;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Lab2 {

    static char[][] board = {{'-','-','-'},{'-','-','-'},{'-','-','-'},};
    private static char currentPlayer ='X';
    private static int row;
    private static int col;
    public static void main(String[] args) {
        printWelcome();
        while (true) {            
        printBoard();
        printTurn();
        inputRowCol();
        if(hasWin()){
            printBoard();
            printWIn();
            break;
        }else if(hasDraw()){
            printBoard();
            printDraw();
            break;
        }
        switchPlayer();
        }
    }

    private static void printWelcome() {
        System.out.println("Welcome to XO"); 
    }
    private static void printBoard() {
        for (int i = 0; i<3;i++) {
            for (int j=0;j<3;j++) {
                System.out.print(board[i][j] + "  ");
            }
            System.out.println();
        }
    }
    private static void printTurn(){
        System.out.println(currentPlayer+" turn");
    }

    private static void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        while (true) {            
         System.out.print("Please Input row col:");
        row = sc.nextInt();
        col = sc.nextInt();
        if(board[row-1][col-1] == '-'){
        board[row-1][col-1]= currentPlayer;
        return;
    }   
        }
    }

    private static void switchPlayer() {
       if(currentPlayer == 'X'){
           currentPlayer = 'O';
       } else{
           currentPlayer= 'X';
       }
    }

    private static boolean hasWin() {
        if(checkRow()||checkCol()||checkX1()||checkX2()){
            return true; 
        } return false;
    }

    private static void printWIn() {
        System.out.println("Player "+currentPlayer+"  Win!!!");
    }

    private static boolean checkRow() {
        for(int i=0;i<3;i++){
            if(board[row-1][i]!=currentPlayer) return false;
        }
        return true;
    }

    private static boolean checkCol() {
        for(int i=0;i<3;i++){
            if(board[i][col-1]!=currentPlayer) return false;
        }
        return true;
    }

    private static boolean checkX1() {
        for(int i=0;i<3;i++){
            if(board[i][i]!=currentPlayer) return false;
        }
        return true;
    }

    private static boolean checkX2() {
        for (int i = 0; i < 3; i++) {
            if (board[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean hasDraw() {
        for (int i = 0; i<3;i++) {
            for (int j=0;j<3;j++) {
                if(board[i][j] == '-' ) return false;
                }
        }
        return  true;
}

    private static void printDraw() {
        System.out.println("It's draw!!");
    }
}
